import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';
import { DisplayComponent } from './display/display.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CarouselComponent } from './carousel/carousel.component';
import { NguCarouselModule } from '@ngu/carousel';
import { MatButtonModule } from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DisplayWordsComponent } from './display-words/display-words.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { WordsTableComponent } from './display-words/words-table.component';
import { WordsGraphComponent } from './display-words/words-graph.component';

@NgModule({
  declarations: [SearchComponent, DisplayComponent, CarouselComponent, DisplayWordsComponent, WordsTableComponent, WordsGraphComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    NguCarouselModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    HttpClientModule
  ],
  exports: [
    SearchComponent,
    DisplayComponent
  ]
})
export class CrawModule { }
