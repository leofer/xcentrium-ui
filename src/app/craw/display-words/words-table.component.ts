import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { WordsCount } from '../words-count';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-words-table',
  templateUrl: './words-table.component.html',
  styleUrls: ['./words-table.component.scss']
})
export class WordsTableComponent implements OnInit {
  @Input() words: WordsCount[];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['word', 'count'];
  dataSource: MatTableDataSource<WordsCount>;

  constructor() { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<WordsCount>(this.words);
    this.dataSource.paginator = this.paginator;
  }
}
