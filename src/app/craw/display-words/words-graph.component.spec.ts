import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordsGraphComponent } from './words-graph.component';

describe('WordsGraphComponent', () => {
  let component: WordsGraphComponent;
  let fixture: ComponentFixture<WordsGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordsGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordsGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
