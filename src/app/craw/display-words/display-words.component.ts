import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { WordsCount } from '../words-count';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-display-words',
  templateUrl: './display-words.component.html',
  styleUrls: ['./display-words.component.scss']
})
export class DisplayWordsComponent implements OnInit {

  @Input() words: WordsCount[];
  constructor() { }

  ngOnInit() {}

}
