import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { WordsCount } from '../words-count';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-words-graph',
  templateUrl: './words-graph.component.html',
  styleUrls: ['./words-graph.component.scss']
})
export class WordsGraphComponent implements OnInit, AfterViewInit {
  @Input() words: WordsCount[];
  @ViewChild('wordsChart') chart: ElementRef;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    const ctx = this.chart.nativeElement.getContext('2d');
    const charValues = this._getValues(this.words);

    const myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        labels: charValues.words,
        datasets: [
          {
            label: 'Words',
            data: charValues.counts,
            backgroundColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)',
              'rgba(154, 201, 205, 1)',
              'rgba(225, 80, 82, 1)'
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: false
      }
    });
  }

  // rest sum minus largest value
  private _getValues(arr: WordsCount[]): { words: string[]; counts: number[] } {
    const totalIndex = arr.findIndex(x => x.word.toUpperCase() === 'TOTAL');
    // const total = arr[totalIndex];

    const total = arr.splice(totalIndex, 1);
    const otherValue = arr.reduce((prev, curr) => {
      return {
        count: prev.count - curr.count,
        word: 'OTHER WORDS'
      };
    }, total[0]);

    arr.push(otherValue);

    return {
      words: arr.map(x => x.word),
      counts: arr.map(x => x.count)
    };
  }
}
