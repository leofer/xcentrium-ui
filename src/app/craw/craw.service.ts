import { Injectable } from '@angular/core';
import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { Craw } from './craw';
import { delay, map, tap } from 'rxjs/operators';
import { looseIdentical } from '@angular/core/src/util';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CrawRequest } from './craw-request';

@Injectable({
  providedIn: 'root'
})
export class CrawService {
  websiteData: BehaviorSubject<Craw>;


  constructor(private http: HttpClient) {
    this.websiteData = new BehaviorSubject<Craw>(null);
  }

  CrawWeb(webSiteUrl: string): Observable<Craw> {
    this.websiteData.next(null);

    const endpoint = environment.apiEndpoint;
    const wordsToTakeNumber = environment.wordsToTake;

    const request: CrawRequest = {
      url: webSiteUrl,
      wordsToTake: wordsToTakeNumber
    };

    return this.http.post<Craw>(endpoint, request).pipe(
      tap(x => {
        this.websiteData.next(x);
      })
    );
  }
}
