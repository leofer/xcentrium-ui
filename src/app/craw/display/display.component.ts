import { Component, OnInit } from '@angular/core';
import { CrawService } from '../craw.service';
import { tap, map, filter } from 'rxjs/operators';
import { WordsCount } from '../words-count';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {
  images: string[];
  words: WordsCount[];

  constructor(public crawService: CrawService) {}

  ngOnInit() {
    this.crawService.websiteData.pipe(
      filter(x => x !== null)
    ).subscribe(data => {
      this.words = data.wordsCount;
      this.images = data.images;
    });
  }

  // getImages() {
  //   return this.crawService.websiteData.pipe(
  //     filter(x => x !== null),
  //     map(x => x.images)
  //   );
  // }

  // getWords() {
  //   return this.crawService.websiteData.pipe(
  //     filter(x => x !== null),
  //     map(x => x.wordsCounts)
  //   );
  // }
}
