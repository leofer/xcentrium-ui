import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CrawService } from '../craw.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  loading = false;
  error: string = null;
  websiteForm = new FormGroup({
    url: new FormControl('', Validators.required)
  });

  constructor(private _crawService: CrawService) {}

  ngOnInit() {}

  onSubmit() {
    if (this.websiteForm.invalid) {
      return;
    }
    this.error = null;
    const controller = this.websiteForm.get('url');
    const url = controller.value;
    this.loading = true;
    this._crawService.CrawWeb(url).subscribe(
      () => {
        this.loading = false;
      },
      err => {
        const errorMessage = err.error !== undefined && err.error !== null ? `: "${err.error}"` : ``;
        this.error = `We encountered an error crawling the site${errorMessage}`;
        this.loading = false;
      }
    );
  }
}
