import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { CrawModule } from '../craw/craw.module';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    CrawModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule { }
